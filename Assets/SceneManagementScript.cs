﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagementScript : MonoBehaviour
{
    public GameObject Transition;
	
	public void GoToQuest(string SceneToLoad)
    {
        StartCoroutine(GoQuest(SceneToLoad));
    }

    IEnumerator GoQuest(string SceneToLoad)
    {
        Transition.GetComponent<Animator>().Play("TransitionOn");

        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene(SceneToLoad);
    }    
}