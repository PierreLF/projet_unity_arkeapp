﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Clock : MonoBehaviour {

    Text clockText;

	// Use this for initialization
	void Start () {
        clockText = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        var today = System.DateTime.Now;
        clockText.text = today.ToString("HH:mm");
    }
}
