﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCredits : MonoBehaviour {

    public GameManagerScript GmScript;

    Text Creds;

    private void Start()
    {
        Creds = GetComponent<Text>();
    }

    private void Update()
    {
        if(GmScript == null)
        {
            GmScript = GameObject.FindObjectOfType<GameManagerScript>();
        }
        else
        {
            Creds.text = " : " + GmScript.Credits;
        }
    }

}
