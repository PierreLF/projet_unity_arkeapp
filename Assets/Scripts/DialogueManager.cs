﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public float delay = 0.1f;

    public Text boiteDialogue;

    string currentText;

    public Text boiteReponse;

    public Animator boiteReponseAnim;

    public Animator Personnage;

    private Queue<string> PhrasesPerso;

    private Queue<string> PhrasesRéponse;

    public GameManagerScript GmManager;

    private void Start()
    {
        PhrasesPerso = new Queue<string>();

        PhrasesRéponse = new Queue<string>();
    }

    public void StartDialogue (Dialogue dialogue)
    {
        Debug.Log("starting " + dialogue.Perso);

        PhrasesPerso.Clear();

        PhrasesRéponse.Clear();

        foreach (string Phrase in dialogue.PhrasesPerso)
        {
            PhrasesPerso.Enqueue(Phrase);
        }

        foreach (string Phrase in dialogue.PhrasesRéponse)
        {
            PhrasesRéponse.Enqueue(Phrase);
        }

        EcrireNextPhrase();
    }

    public void EcrireNextPhrase()
    {
        if (PhrasesPerso.Count == 0)
        {
            EndDialogue();
            return;
        }

        string Phrase = PhrasesPerso.Dequeue();

        StartCoroutine(NextCaracter(Phrase));
    }

    void CurrentPhraseOver()
    {
        if (PhrasesRéponse.Count == 0)
        {
            EndDialogue();
            return;
        }

        boiteReponseAnim.Play("BulleReponseAppear");

        string Phrase = PhrasesRéponse.Dequeue();
        boiteReponse.text = Phrase;
    }

    void EndDialogue()
    {
        GmManager.EndDialogue();

        Personnage.Play("CaracterIconDisappear");
    }

    IEnumerator NextCaracter(string Phrase)
    {
        boiteDialogue.text = "";

        for (int i = 0; i < Phrase.Length; i++)
        {
            currentText = Phrase.Substring(0, i + 1);
            boiteDialogue.text = currentText;

            if (i == Phrase.Length - 1)
            {
                CurrentPhraseOver();
            }

            yield return new WaitForSeconds(delay);
        }
    }
}
