﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManagerScript : MonoBehaviour {

    public GameManagerScript GmManager;

    public string OpenedMenu;

    public GameObject QuestMenu;
    public GameObject InventoryMenu;
    public GameObject OfficeMenu;
    public GameObject OnlineMenu;

    public Animator QuestMenuAnim;
    public Animator InventoryMenuAnim;
    public Animator OfficeMenuAnim;
    public Animator OnlineMenuAnim;

    public void OpenQuestMenu()
    {
        QuestMenu.SetActive(true);

        OpenedMenu = QuestMenu.name;

        QuestMenuAnim.Play("PanelPopUp");
    }

    public void OpenInventoryMenu()
    {
        InventoryMenu.SetActive(true);

        OpenedMenu = InventoryMenu.name;

        InventoryMenuAnim.Play("PanelPopUp");
    }

    public void OpenOfficeMenu()
    {
        OfficeMenu.SetActive(true);

        OpenedMenu = OfficeMenu.name;

        OfficeMenuAnim.Play("PanelPopUp");
    }

    public void OpenOnlineMenu()
    {
        OnlineMenu.SetActive(true);

        OpenedMenu = OnlineMenu.name;

        OnlineMenuAnim.Play("PanelPopUp");
    }

    public void CloseCurrentMenu()
    {
        
    }
}