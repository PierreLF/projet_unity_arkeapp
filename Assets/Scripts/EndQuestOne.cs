﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndQuestOne : MonoBehaviour {

    public Datas datas;

	void Start ()
    {
        datas = GameObject.FindObjectOfType<Datas>();

    }

    public void Rewards ()
    {
        datas.Credits = datas.Credits + 10;

        datas.Quest1Completed = true;
    }
}