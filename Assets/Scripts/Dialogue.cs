﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    public string Perso;

    [TextArea(2,3)]
    public string[] PhrasesPerso;

    [TextArea(2, 3)]
    public string[] PhrasesRéponse;
}
