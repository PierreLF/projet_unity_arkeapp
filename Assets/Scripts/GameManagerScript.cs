﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour {

	public string UserName;

    public GameObject EntreeDuNom;

    public Datas datas;

    public InputField NameInputField;

    public Text BrjText;

    public int Credits;

    public Animator QuestCaracter;

    public Animator boitePersoAnim;

    public int CurrentQuestID;

    public GameObject Vetements_GM;
    public GameObject lunettes;
    public GameObject Vetements_Pompiers;

    public bool LaunchQuest;

    public bool CurrentQuestOver;

    public QuestDialogue Dialogue1;

    public QuestDialogue Dialogue2;

    public QuestDialogue Dialogue3;

    public bool Quest1Completed;

    public bool Quest2Completed;

    bool Quest1CompletedDoOnce;

    void Start ()
    {
        datas = FindObjectOfType<Datas>();
    }

    public void AssignName()
    {
        UserName = NameInputField.text;

        BrjText.text = " Bonjour " + UserName;

        datas.UserName = UserName;

        if (CurrentQuestID == 0)
        {
            StartCoroutine(StartAll());
        }
    }

	void Update () {

        if (LaunchQuest && CurrentQuestOver)
        {
            LaunchQuest = false;

            StartCoroutine(QuestInquiery());
        }

        if(BrjText == null)
        {
            BrjText = GameObject.FindGameObjectWithTag("Name").GetComponent<Text>();
        }

        if (Quest1Completed && !Quest1CompletedDoOnce)
        {
            lunettes.SetActive(true);

            StartCoroutine(QuestInquiery());

            Quest1CompletedDoOnce = true;
        }

        if (CurrentQuestID != 0)
        {
            EntreeDuNom.SetActive(false);
        }
    }

    public IEnumerator StartAll()
    {
        yield return new WaitForSeconds(10);

        StartCoroutine(QuestInquiery());
    }

    public void EndDialogue()
    {
        CurrentQuestOver = true;

        boitePersoAnim.Play("BulleClientDisappear");
    }

    public IEnumerator QuestInquiery()
    {
        CurrentQuestOver = false;

        CurrentQuestID += 1;
        datas.CurrentQuestID = CurrentQuestID;

        if (CurrentQuestID == 1)
        {
            Vetements_GM.SetActive(true);
        }
        if (CurrentQuestID == 2)
        {
            Vetements_GM.SetActive(true);
        }

        yield return new WaitForSeconds(1);

        QuestCaracter.Play("CaracterIconAppear");

        yield return new WaitForSeconds(0.5f);

        boitePersoAnim.Play("BulleClientAppear");

        yield return new WaitForSeconds(0.5f);

        if (CurrentQuestID == 1)
        {
            Dialogue1.TriggerDialogue();
        }
        if (CurrentQuestID == 2)
        {
            Dialogue2.TriggerDialogue();
        }
    }
}