﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Datas : MonoBehaviour {

    public GameManagerScript GmManager;

    public string UserName;

    public int CurrentQuestID;

    public bool Quest1Completed;

    public bool Quest2Completed;

    public int Credits;

    void Update ()
    {
        if(GmManager == null)
        {
            GmManager = GameObject.FindObjectOfType<GameManagerScript>();

            return;
        }
        else
        {
            GmManager.UserName = UserName;

            GmManager.CurrentQuestID = CurrentQuestID;

            GmManager.Quest1Completed = Quest1Completed;

            GmManager.Quest2Completed = Quest2Completed;

            GmManager.Credits = Credits;
        }
	}
}