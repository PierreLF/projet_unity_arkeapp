﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObj : MonoBehaviour {

    float offsetX;

    float offsetY;

    public void BeginDrag()
    {
        offsetX = transform.position.x - Input.mousePosition.x;
        offsetY = transform.position.y - Input.mousePosition.y;

        transform.localScale = transform.localScale * 1.2f;
    }

    public void OnDrag()
    {
        transform.position = new Vector3(offsetX + Input.mousePosition.x, offsetY + Input.mousePosition.y);
    }

    public void EndDrag()
    {
        transform.localScale = transform.localScale / 1.2f;
    }
}